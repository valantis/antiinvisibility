package com.electronicboy.AntiInvisibility;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.List;
import java.util.UUID;

public class AntiInvisibility extends JavaPlugin implements Listener {

    private String perm = "AntiInvisibility.exempt";

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);

        List<Entity> entities = Bukkit.getServer().getWorld("world").getEntities();

        for (Entity ent : entities) {
            if (ent instanceof LivingEntity) {
                if (((LivingEntity) ent).hasPotionEffect(PotionEffectType.INVISIBILITY)) {
                    ((LivingEntity) ent).removePotionEffect(PotionEffectType.INVISIBILITY);
                }
            }
        }



    }


    @EventHandler
    public void OnDrink(PlayerItemConsumeEvent event) {
        ItemStack im = event.getPlayer().getItemInHand();
        if (im.getType() == Material.POTION) {

            Potion potion = Potion.fromItemStack(im);
            if (potion.getType() == PotionType.INVISIBILITY) {
                final UUID Player = event.getPlayer().getUniqueId();
                if (!event.getPlayer().hasPermission(perm)) {
                    AntiInvisRun(Player);
                }
            }
        }
    }

    @EventHandler
    public void onSplash(PotionSplashEvent event) {
        if (event.getPotion().getEffects().contains(PotionEffectType.INVISIBILITY))
            event.setCancelled(true);

        /*
        for (final LivingEntity p: event.getAffectedEntities()) {
                if (p instanceof Player) {

                    if (p.hasPermission(perm)) {
                        this.getServer().getScheduler().runTaskLater(this, new Runnable() {
                            @Override
                            public void run() {
                                if (getServer().getPlayer(p).hasPotionEffect(PotionEffectType.INVISIBILITY) && getServer().getPlayer(p).isOnline()) {
                                    getServer().getPlayer(p).removePotionEffect(PotionEffectType.INVISIBILITY);
                                }
                            }
                        }, 1L);
                    }
                }
            }
            */
    }


    public void onLogin(PlayerLoginEvent event) {
        if (!event.getPlayer().hasPermission(perm)) {
            event.getPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);
        }
    }

    private void AntiInvisRun(UUID PlayerUUID) {

        final UUID Player = PlayerUUID;

        if (!getServer().getPlayer(Player).hasPermission(perm)) {
            this.getServer().getScheduler().runTaskLater(this, new Runnable() {
                @Override
                public void run() {
                    if (getServer().getPlayer(Player).hasPotionEffect(PotionEffectType.INVISIBILITY) && getServer().getPlayer(Player).isOnline()) {
                        getServer().getPlayer(Player).removePotionEffect(PotionEffectType.INVISIBILITY);
                    }
                }
            }, 1L);
        }

    }

}
